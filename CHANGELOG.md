# Change Log

## [0.4.2]

- Nouveau logo

## [0.4.1]

- Ajout des commentaires sur une ligne et plusieurs lignes (avec // et /**/)

## [0.4.0]

- Ajout de l'autocomplétion (partiellement)

## [0.3.1]

- Correction de certaines synthaxes

## [0.3.0]

- Ajout des structures

- Ajout de nouveaux snippets: programme et structure

## [0.2.0]

- J'ai changé le README.md :enorme:

## [0.1.0]

- Initial release
