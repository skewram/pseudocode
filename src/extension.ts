import * as vscode from 'vscode';

export function activate(ctx: vscode.ExtensionContext){
    const provider = vscode.languages.registerCompletionItemProvider('pseudo-code', {

        provideCompletionItems(doc: vscode.TextDocument, pos: vscode.Position, token: vscode.CancellationToken, ctx: vscode.CompletionContext){

            /* Types */
            let types: Array<vscode.CompletionItem> = [];
            if((doc.lineAt(pos).text.substring(0, pos.character).startsWith("constante") || doc.lineAt(pos).text.substring(0, pos.character).match(/(:( )?)/g)) && !doc.lineAt(pos).text.substring(0, pos.character).match(/(:=)/g)){
                types.push(new vscode.CompletionItem("réel", vscode.CompletionItemKind.Interface));
                types.push(new vscode.CompletionItem("entier", vscode.CompletionItemKind.Interface));
                types.push(new vscode.CompletionItem("chaîne", vscode.CompletionItemKind.Interface));
                types.push(new vscode.CompletionItem("booléen", vscode.CompletionItemKind.Interface));
                types.push(new vscode.CompletionItem("tableau", vscode.CompletionItemKind.Interface));
            }

            /* Functions */
            let functions: Array<vscode.CompletionItem> = [];
            
            /* Keywords */
            let keyword: Array<vscode.CompletionItem> = [];
            if(doc.lineAt(pos).isEmptyOrWhitespace){
                keyword.push(new vscode.CompletionItem("fonction", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("procédure", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("programme", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("constante", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("type", vscode.CompletionItemKind.Keyword));
            }
            if(doc.lineAt(pos).text.substring(0, pos.character).replace(/ /g, '').endsWith("(") || doc.lineAt(pos).text.substring(0, pos.character).replace(/ /g, '').endsWith(",")){
                if(doc.lineAt(pos).text.substring(0, pos.character).startsWith("fonction") || doc.lineAt(pos).text.substring(0, pos.character).startsWith("procédure")){
                    keyword.push(new vscode.CompletionItem("entF", vscode.CompletionItemKind.Keyword));
                    keyword.push(new vscode.CompletionItem("sortF", vscode.CompletionItemKind.Keyword));
                    keyword.push(new vscode.CompletionItem("entF/sortF", vscode.CompletionItemKind.Keyword));
                }else{
                    keyword.push(new vscode.CompletionItem("entE", vscode.CompletionItemKind.Keyword));
                    keyword.push(new vscode.CompletionItem("sortE", vscode.CompletionItemKind.Keyword));
                    keyword.push(new vscode.CompletionItem("entE/sortE", vscode.CompletionItemKind.Keyword));
                }
            }else{
                keyword.push(new vscode.CompletionItem("structure", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("si", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("sinonsi", vscode.CompletionItemKind.Keyword));
                keyword.push(new vscode.CompletionItem("sinon", vscode.CompletionItemKind.Keyword));
                
                /* Functions */
                const writeOnScreen = new vscode.CompletionItem("écrireEcran", vscode.CompletionItemKind.Function);
                writeOnScreen.insertText = new vscode.SnippetString("écrireEcran($1);");
                const readKeyboard = new vscode.CompletionItem("lireClavier", vscode.CompletionItemKind.Function);
                readKeyboard.insertText = new vscode.SnippetString("lireClavier($1);");
                functions.push(writeOnScreen);
                functions.push(readKeyboard);                
            }

            return new Array<vscode.CompletionItem>().concat(types, keyword, functions);
        }

    });
    ctx.subscriptions.push(provider);
}

export function deactivate(){}