# PSEUDO CODE

## > **Nouvelle mise à jour après 1 an !**

![update](https://i.ibb.co/BBfQdTx/new-update.png)

L'extension dont vous rêviez est là !

La première extension supportant le pseudo code tant chéri pour notre chère IUT Bretonne (#galettesaucisse)

Des fonctionnalités qui dépassent le champs de possibles (référence à la publicité des BN qui veut dire soit dit en passant Biscuiterie Nantaise).

## Fonctionnalités

- Coloration synthaxique

- Commentaires

- Snipets:
  - Conditions
  - Boucles (pour, tant que, répéter)
  - Fonctions
  - Procédures
  - Structures
